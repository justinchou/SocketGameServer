import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("Socket", process.pid, __filename);

// import HeapDump from "heapdump";

import Server from "../server";

var host = process.argv[4] || "127.0.0.1";
var port = process.argv[3] || -1;
var serverId = process.argv[2] || -1;

if (serverId < 0 || serverId > 65535 || port < 0 || port > 65535) {
    Logger.error("Start Server With Invalid Server ID [ %s ] OR port [ %s ]!!", serverId, port);
    process.exit(-1);
}

var server = new Server(serverId, port, host);
server.start();
Logger.info("Socket Server [ %s:%s ] Started.", host, port);

process.on("SIGINT", function() {
    Logger.info('Got SIGINT');
    process.exit(0);
});

process.on("message", function(m) {
    if (m == "kill") {
        let filename = __dirname + "/../../log/heapsnapshot-" + Date.now() + ".log";
        // HeapDump.writeSnapshot(filename, () => { process.exit(0); });
        process.exit(0);
    }
});

process.on("exit", function(code) {
    setTimeout(function() {
        Logger.warn("This Will NOT Run");
    }, 0);

    Logger.info("Server [ %s:%s ] About To Exit With Code:", host, port, code);

    if (server) server.stop();
});

process.on("uncaughtException", function(err) {
    Logger.error("Server Got uncaughtException " + err.stack);
});