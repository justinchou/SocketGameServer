import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("Manager", process.pid, __filename);

import Manager from "../manager";

let manager = new Manager();
manager.start();
Logger.info("Manager Server [ %s:%s ] Started.", manager._host, manager._port);

process.on("exit", function(code) {
    setTimeout(function() {
        Logger.warn("This Will NOT Run");
    }, 0);

    Logger.info("Server [ %s:%s ] About To Exit With Code:", manager._host, manager._port, code);
});

process.on("uncaughtException", function(err) {
    Logger.error("Manager Server Got uncaughtException " + err.stack);
});