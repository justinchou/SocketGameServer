import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("Master", process.pid, __filename);

import Master from "../master";

let master = new Master();
master.start();
Logger.info("Master Server [ %s:%s ] Started.", master._host, master._port);

process.on("exit", function(code) {
    setTimeout(function() {
        Logger.warn("This Will NOT Run");
    }, 0);

    Logger.info("Server [ %s:%s ] About To Exit With Code:", master._host, master._port, code);
});

process.on("uncaughtException", function(err) {
    Logger.error("Master Server Got uncaughtException " + err.stack);
});