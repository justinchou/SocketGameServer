import GameObject from "./gameObject";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("Region", process.pid, __filename);

class Region extends GameObject {
    constructor(regionId){
        super();
        this.regionId = regionId;
        this.rooms    = [];
    }

    set objectId(v){this.objectId = v;}
    set staticId(v){this.staticId = v;}
}

module.exports = Region;