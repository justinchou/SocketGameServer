import SeedRandom from "seedrandom";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("GameObject", process.pid, __filename);

class GameObject {
    constructor() {
        this.gameServer = null;
        this.socket     = null;

        this.objectId   = null;
        this.staticId   = null;

        this._random    = SeedRandom("Region:" + this.objectId + ":" + this.staticId);
    }

    get objectId() { return this.objectId; }
    set objectId(v) { throw(new Error("GameObject Interface [ set objectId ] Need To Be Realized")); }

    get staticId() { return this.staticId; }
    set staticId(v) { throw(new Error("GameObject Interface [ set staticId ] Need To Be Realized")); }

    get random() { return this._random(); }
    set random(v) { throw(new Error("Parameter random Is Read-Only, Cannot Be Set To: " + v)); }
    getRandom() { return this.random; }
}

module.exports = GameObject;
export default GameObject;