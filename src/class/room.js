import GameObject from "./gameObject.js";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("Room", process.pid, __filename);

class Room extends GameObject {
    constructor(roomId){
        super();
        this.roomId = roomId;
        this.users  = [];
    }

    set objectId(v){this.objectId = v;}
    set staticId(v){this.staticId = v;}
}

module.exports = Room;