import GameObject from "./gameObject.js";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("User", process.pid, __filename);

class User extends GameObject {
    constructor(userId){
        super();
        this.userId = userId;
    }

    set objectId(v){this.objectId = v;}
    set staticId(v){this.staticId = v;}
}

module.exports = User;
