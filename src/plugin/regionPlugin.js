import GamePlugin from "./gamePlugin";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("RegionPlugin", process.pid, __filename);

class RegionPlugin extends GamePlugin {
    constructor(gameServer){
        super(gameServer);
        this.regions = [];
    }

    configure() {}
    start() {}
    update() {}
    stop() {}
}

module.exports = RegionPlugin;
export default RegionPlugin;
