import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("GamePlugin", process.pid, __filename);

class GamePlugin {
    constructor(pluginId) {
        this.pluginId = pluginId;

        this._STATUS = {
            "CREATED"  : 0,
            "INITIALIZING"  : 1,
            "INITIALIZED"   : 2,
            "STARTING" : 3,
            "STARTED"  : 4,
            "STOPPING"  : 5,
            "STOPPED"   : 6
        };
        this.status = this._STATUS.CREATED;

        this._time      = (new Date()).getTime(); // Time Counter
        this._startTime = this._time;

        this.tick         = 0;
        this.tickTick     = 0;
        this.tickTickTick = 0;
    }

    setStatus(code) {
        let status = this._STATUS[code] || -1;
        if (status !== -1) this.status = status;
    }
    getStatus() {
        return this.status;
    }
    isStatus(code) {
        let status = this._STATUS[code] || 0;
        return this.status === status;
    }
    set time(v) {}
    get time() {return this._time;}
    set startTime(v) {}
    get startTime() {return this._startTime;}

    configure() {
        if (!this.isStatus("CREATED")) return;
        this.setStatus("INITIALIZING");

        Logger.debug("Write Configure Code !!!HERE!!!");

        this.setStatus("INITIALIZED");

        throw(new Error("GamePlugin Interface [ configure ] Need To Be Realized"));
    }
    start() {
        if (this.isStatus("CREATED")) this.configure();

        if (!this.isStatus("INITIALIZED")) return;
        this.setStatus("STARTING");

        Logger.debug("Write Start Code !!!HERE!!!");

        this.setStatus("STARTED");

        throw(new Error("GamePlugin Interface [ start ] Need To Be Realized"));
    }
    update() {
        //               !!! WARNING !!!
        //            Add Loop Call If Need
        // ==> setTimeout(this.update.bind(this), 1); <==

        if (!this.isStatus("RUNNING")) return;

        Logger.debug("Write Update Code !!!HERE!!!");

        throw(new Error("GamePlugin Interface [ update ] Need To Be Realized"));
    }
    stop() {
        if (!this.isStatus("STARTED")) return;
        this.setStatus("STOPPING");

        Logger.debug("Write Stop Code !!!HERE!!!");

        this.setStatus("STOPPED");

        throw(new Error("GamePlugin Interface [ stop ] Need To Be Realized"));
    }
}

module.exports = GamePlugin;
export default GamePlugin;