import GamePlugin from "./gamePlugin";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("ConnectionPlugin", process.pid, __filename);

class ConnectionPlugin extends GamePlugin {
    constructor(gameServer) {
        super(gameServer);
        this.connections = [];
    }

    configure() {}
    start() {}
    update() {}
    stop() {}

    getConnectionsAmount() {
        return this.connections.length;
    }
    getAliveConnectionsAmount() {
        var counter = 0;
        this.connections.forEach(connection => {
            // if (connection.??)
                counter += 1;
        });
        return counter;
    }
}

module.exports = ConnectionPlugin;
export default ConnectionPlugin;
