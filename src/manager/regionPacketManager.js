import GameManager from "./gameManager";
import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("RegionPacketManager", process.pid, __filename);

const PacketAPI = require("../../config/packet.api.json");

class RegionPacketManager extends GameManager {
    constructor(gameServer, socket){
        super(gameServer);
        this.socket = socket;
    }

    configure() {}
    start() {}
    update() {}
    stop() {}

    handleMessage(data) {
        if (data.packetId < PacketAPI.MIN_REGION_API || data.packetId > PacketAPI.MAX_REGION_API) return;

        Logger.debug("Region Packet [ %s ] [ %j ]", data.packetId, data);

        switch (data.packetId) {
            case PacketAPI.ON_REGION_ENTER:
                break;
            case PacketAPI.ON_REGION_LEAVE:
                break;
            case PacketAPI.TO_UPDATE_ROOM_LIST:
                break;
        }
    }
}

module.exports = RegionPacketManager;
export default RegionPacketManager;
