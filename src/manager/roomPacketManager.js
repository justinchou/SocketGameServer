import GameManager from "./gameManager";
import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("RoomPacketManager", process.pid, __filename);

const PacketAPI = require("../../config/packet.api.json");

class RoomPacketManager extends GameManager {
    constructor(gameServer, socket){
        super(gameServer);
        this.socket = socket;
    }

    configure() {}
    start() {}
    update() {}
    stop() {}

    handleMessage(data) {
        if (data.packetId < PacketAPI.MIN_ROOM_API || data.packetId > PacketAPI.MAX_ROOM_API) return;

        Logger.debug("Room Packet [ %s ] [ %j ]", data.packetId, data);

        switch (data.packetId) {
            case PacketAPI.ON_ROOM_CREATE:
                break;
            case PacketAPI.ON_ROOM_ENTER:
                break;
            case PacketAPI.ON_ROOM_LEAVE:
                break;
            case PacketAPI.TO_UPDATE_USER_LIST:
                break;
        }
    }
}

module.exports = RoomPacketManager;
export default RoomPacketManager;
