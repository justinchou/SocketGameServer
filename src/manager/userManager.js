import GameManager from "./gameManager";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("UserManager", process.pid, __filename);

class UserManager extends GameManager {
    constructor(gameServer, socket){
        super(gameServer);
        this.socket = socket;

        this.user   = null;
        this.vUsers = [];
    }

    configure() {}
    start() {}
    update() {}
    stop() {}
}

module.exports = UserManager;
export default UserManager;