import GameManager from "./gameManager";
import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("ChatPacketManager", process.pid, __filename);

const PacketAPI = require("../../config/packet.api.json");

class ChatPacketManager extends GameManager {
    constructor(gameServer, socket){
        super(gameServer);
        this.socket = socket;
    }

    configure() {}
    start() {}
    update() {}
    stop() {}

    handleMessage(data) {
        if (data.packetId < PacketAPI.MIN_CHAT_API || data.packetId > PacketAPI.MAX_CHAT_API) return;

        Logger.debug("Chat Packet [ %s ] [ %j ]", data.packetId, data);

        switch (data.packetId) {
            case PacketAPI.ON_SET_NICKNAME:
                break;
            case PacketAPI.ON_SEND_PUBLIC_MESSAGE:
                break;
            case PacketAPI.ON_SEND_PRIVATE_MESSAGE:
                break;
        }
    }
}

module.exports = ChatPacketManager;
export default ChatPacketManager;
