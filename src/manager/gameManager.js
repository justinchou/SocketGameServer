import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("GameManager", process.pid, __filename);

class GameManager {
    constructor(gameServer){
        this.gameServer = gameServer;
    }

    configure() { throw (new Error("GameManager Interface [ configure ] Need To Be Implemented!")); }
    start() {throw(new Error("GameManager Interface [ start ] Need To Be Realized"));}
    update() {throw(new Error("GameManager Interface [ update ] Need To Be Realized"));}
    stop() {throw(new Error("GameManager Interface [ stop ] Need To Be Realized"));}
}

module.exports = GameManager;
export default GameManager;
