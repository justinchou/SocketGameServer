import GameManager from "./gameManager";
import Ws from "ws";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("PacketManager", process.pid, __filename);

import ChatPacketManager from "./chatPacketManager";
import RoomPacketManager from "./roomPacketManager";
import RegionPacketManager from "./regionPacketManager";

const PacketAPI = require("../../config/packet.api.json");

class PacketManager extends GameManager {
    constructor(gameServer, socket){
        super(gameServer);
        this.socket = socket;

        this.chatPacketManager = new ChatPacketManager(gameServer, socket);
        this.roomPacketManager = new RoomPacketManager(gameServer, socket);
        this.regionPacketManager = new RegionPacketManager(gameServer, socket);
    }

    configure() {
        this.chatPacketManager.configure();
        this.roomPacketManager.configure();
        this.regionPacketManager.configure();
    }
    start() {
        this.chatPacketManager.start();
        this.roomPacketManager.start();
        this.regionPacketManager.start();
    }
    update() {
        this.chatPacketManager.update();
        this.roomPacketManager.update();
        this.regionPacketManager.update();
    }
    stop() {
        this.chatPacketManager.stop();
        this.roomPacketManager.stop();
        this.regionPacketManager.stop();
    }

    parseMessage(message) {
        return JSON.parse(message);
    }

    handleMessage(message) {
        var data = this.parseMessage(message);

        if (!data || data == null || typeof data != "object" || !data.hasOwnProperty("packetId")) return;

        if (data.packetId > PacketAPI.MIN_DEFAULT_API && data.packetId < PacketAPI.MAX_DEFAULT_API) {
            switch (data.packetId) {
                case PacketAPI.ON_CONNECTION:
                    break;
                case PacketAPI.ON_DISCONNECT:
                    break;
                case PacketAPI.ON_HEART_BEAT:
                    if (this.gameServer && this.gameServer.heartbeatPlugin) {
                        Logger.debug("Receive Heartbeat Packet [ %s ] [ %j ]", data.packetId, data);
                        this.gameServer.heartbeatPlugin.heartbeat(data);
                    }
                    break;
            }
        } else
        if (data.packetId > PacketAPI.MIN_REGION_API && data.packetId < PacketAPI.MAX_REGION_API) {
            this.regionPacketManager.handleMessage(data);
        } else
        if (data.packetId > PacketAPI.MIN_ROOM_API && data.packetId < PacketAPI.MAX_ROOM_API) {
            this.roomPacketManager.handleMessage(data);
        } else
        if (data.packetId > PacketAPI.MIN_CHAT_API && data.packetId < PacketAPI.MAX_CHAT_API) {
            this.chatPacketManager.handleMessage(data);
        }
    }
}

module.exports = PacketManager;
export default PacketManager;

Ws.prototype.sendPacket = function (packetId, message) {
    // Todo Encrypt Message
    if (this.readyState == Ws.OPEN) {
        if (!message) message = {};

        message.packetId = packetId;
        var data = JSON.stringify(message);
        // Logger.debug("Send Server Message  [ %j ]", data);
        this.send(data, function (err) {
            if (err) Logger.error("Server Send Data To Client Return Error: ", err);
        });
    } else {
        this.readyState = Ws.CLOSED;
        this.emit('close');
        this.removeAllListeners();
    }
};

// Ws.prototype.sendPrivatePacket = function (packetId, ws, message) {
//     ws.sendPacket(packetId, message);
// };
//
// Ws.prototype.sendBroadcastPacket = function (packetId, wss, message) {
//     wss.forEach(ws => {ws.sendPacket(packetId, message);});
// };