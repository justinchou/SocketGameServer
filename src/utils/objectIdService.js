import SeedRandom from "seedrandom";

import PomeloLogger from "pomelo-logger";
const Logger = PomeloLogger.getLogger("ObjectIdService", process.pid, __filename);

class ObjectIdService {
    constructor(){
        this._inst = null;

        this._objectId = 0;
        this._random   = SeedRandom("ObjectIdService");
    }

    static getInstance(){
        if (this._inst == null) {
            this._inst = new ObjectIdService();
        }
        return this._inst;
    }

    get inst(){ throw(new Error("Instance Service [ ObjectId Service ] Cannot Load Instance Directly!")); }
    set inst(v){
        if (this._inst instanceof ObjectIdService) return;
        if (this._inst == null && v instanceof ObjectIdService) {
            this._inst = v;
        }
    }


    get objectId() { throw(new Error("Cannot Get objectId Directly With *.objectId, Please Use ObjectIdService.getInstance().getNewObjectId()")); }
    set objectId(v) { throw(new Error("Cannot Set objectId Manually With *.objectId = " + v)); }
    getNewObjectId() {
        this._objectId += 1;
        return this._objectId;
    }


    get random() { throw(new Error("Cannot Get random Directly With *.random, Please Use ObjectIdService.getInstance().getRandom()")); }
    set random(v) { throw(new Error("Cannot Set random Manually With *.random = " + v)); }
    getRandom() {
        return this._random();
    }
}

module.exports = ObjectIdService;
