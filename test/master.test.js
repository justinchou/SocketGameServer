"use strict";

const RQ     = require("request");
const Async  = require("async");
const Util   = require("util");
const Logger = require("pomelo-logger").getLogger("Master.Test", __filename);

const MasterConfig = require("../config/master.config.json");
var URL = Util.format("http://%s:%s", MasterConfig.host, MasterConfig.master.port);

var ports = [];

function parse(body) {
    var ret;
    try {
        ret = JSON.parse(body);
    } catch (e) {
        Logger.error("Parse JSON Data [ %s ] Failed!", body);
        throw (e);
    }
    return ret;
}

Async.waterfall([
    // Start Server
    function (next) {
        Logger.info(Util.format("%s/%s",URL, "startNServer"), {"form":{"username":"Justin","password":"123456", "n": 3}});
        RQ.post(Util.format("%s/%s",URL, "startNServer"), {"form":{"username":"Justin","password":"123456", "n": 3}}, (err, res, body) => {
            if (err) return next(err);

            ports = parse(body).ports;
            if (ports.length <= 0) return next(new Error("Start Server Failed"));

            Logger.debug("Start Servers [ %j ]", parse(body).ports);

            next();
        });
    },
    // Load Server Status
    function (next) {

    },
    // Restart One Server
    function (next) {
        Logger.info(Util.format("%s/%s",URL, "restartServer"), {"form":{"username":"Justin","password":"123456", "port": ports[0]}});
        RQ.post(Util.format("%s/%s",URL, "restartServer"), {"form":{"username":"Justin","password":"123456", "port": ports[0]}}, (err, res, body) => {
            if (err) return next(err);
            next();
        });
    },
    // Stop One Server
    function (next) {
        Logger.info(Util.format("%s/%s",URL, "killServer"), {"form":{"username":"Justin","password":"123456", "port": ports[0]}});
        RQ.post(Util.format("%s/%s",URL, "killServer"), {"form":{"username":"Justin","password":"123456", "port": ports[0]}}, (err, res, body) => {
            if (err) return next(err);
            ports = parse(body).ports;
            next();
        });
    },
    // Stop All Server
    function (next) {
        Logger.info(Util.format("%s/%s",URL, "killAllServer"), {"form":{"username":"Justin","password":"123456"}});
        RQ.post(Util.format("%s/%s",URL, "killAllServer"), {"form":{"username":"Justin","password":"123456"}}, (err, res, body) => {
            if (err) return next(err);

            Logger.debug("Killed Servers [ %j ]", parse(body).ports);

            next();
        });
    }
], function (err) {
    if (err) Logger.error("Master Test Failed: ", err);

    setTimeout(function () {
        Logger.info(Util.format("http://%s:%s/", ServerConfig.host, ServerConfig.master.port + ServerConfig.monitor.addPort));
        RQ(Util.format("http://%s:%s/", ServerConfig.host, ServerConfig.master.port + ServerConfig.monitor.addPort),(err, res, body) => {
            Logger.info(body);
        });
    }, 5000);
});

