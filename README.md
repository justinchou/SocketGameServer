#SocketGameServer

## Quick Start

### 1. 下载源代码并且安装依赖


    git clone git@git.oschina.net:justinchou/SocketGameServer.git
    cd SocketGameServer
    npm install -d


### 2. 执行编译与启动


    npm run buildstart manager
    npm run buildstart master

### 3. 执行测试用例[TODO]


    npm run test


## 自动化管理工具

### 1.NPM


代码编译

    npm run build

运行测试用例[TODO]

    npm run test
    
Manager 服务器的启动

    npm run buildstart manager
    npm run start manager

Master 服务器的启动

    npm run buildstart master
    npm run start master

使用Master网页后台管理工具管理Server[TODO]

    # 启动
    curl http://ip:port/startNServer -d username=Justin&password=123456&n=3
    
    # 重启
    curl http://ip:port/restartServer -d username=Justin&password=123456&port=15001
    
    # 重启所有
    curl http://ip:port/restartAllServer -d username=Justin&password=123456
    
    # 关闭
    curl http://ip:port/killServer -d username=Justin&password=123456&port=15001
    
    # 关闭所有
    curl http://ip:port/killAllServer -d username=Justin&password=123456

### 2. Gulp

### 3. Grunt


## 服务器架构

### 1. managerIns.js 服务器心跳监听, 通过 bin/managerIns 启动

所有的serverIns启动后都会与本服务器建立心跳链接, 超时的服务器将会发送重启命令到对应的masterIns进行重启管理

Note: 可能是分布在多个服务器上的多个masterIns共同使用同一个managerIns.

### 2. masterIns.js 服务器管理类, 通过 bin/masterIns 启动

启动主文件, 本文件内部的Http服务, 负责管理"服务器类 serverIns.js"子进程的启动重启.

Note: 一般一个服务器主机有一个masterIns, 管理该服务器上所有serverIns.

### 3. serverIns.js 服务器类, 通过manager管理类 (处理逻辑为: 对 bin/serverIns 文件传递参数, 进行启动重启)

单独启动而不使用master进行管理(可以使用port作为serverId)

    node bin/serverIns serverId port host 
    
小结与拓展:
Manager类, Master类 和 Server类都是继承自 GamePlugin 基类的, 相当于是一个服务器组件的概念, 
每个组件都有对应的 configure, start, update, stop 四种方法, 
configure用于加载组件的配置文件, 或初始化一些数据, 抑或重置一些数据,
start用于在加载完configure之后, 执行启动逻辑将组件标记到Running状态,
update用于执行组件心跳方法, 内部写的逻辑都是需要定时执行的,
stop是在组件销毁之前执行的方法, 用于对象回收或是数据存档等.
每个组件可以独立成为一个服务, 也可以作为其他组件的属性方式存在, 只要维护好四个方法在合适地方进行调用即可.

### 4. Plugin 组件类


### 5. Manager 管理类


### 6. Class 对象类


### 7. Utils 服务类


## 配置文件

配置文件均存放在src上级目录, 方便编译后与新生成的build目录有相同的文件加载路径.

### 1. global.config

多种服务器可能都需要的配置

### 2. manager.config

Manager 服务器需要的配置文件

### 3. master.config

Master 服务器需要的配置文件

### 4. server.config

Server 服务器需要的配置文件

### 5. packet.api

收发的数据包对应的接口packetId


## Test 自动化测试类

目前自动化测试都是需要依存于服务器启动后再运行 node test/xx.test.js 文件进行测试的,
需要完善成可以不手动启动服务器, 直接运行grant/gulp/mocha等即可自动化测试生成测试报告的方法.


## 代码覆盖率等